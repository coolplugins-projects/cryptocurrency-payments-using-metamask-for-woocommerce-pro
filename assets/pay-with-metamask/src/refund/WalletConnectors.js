import React, { useState, useEffect } from "react";
import { WagmiProvider,useAccount } from "wagmi";
import { ConnectKitProvider,useModal } from "connectkit";
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import {  
  handleSwitchNetworkMessage,
  createCustomConfig
} from "../component/helper";
import SendnativeCurrency from "./sendNativeCurrency";
import SendToken from "./sendToken";

const {
  const_msg,
  currency_symbol,
  default_currency,
  selected_wallet,
  decimalchainId,
  ccpw_wc_id,
  network_data,
  label,
} = extradata;

const Checkcurrency = ({ clicked , switchHandler ,switchModal}) => {
  const { chain } = useAccount();
  const { isConnected } = useAccount();
  const { open, setOpen , openSwitchNetworks } = useModal();

  useEffect(() => {
    if (chain?.id == decimalchainId) {
      setOpen(false);
    }
  }, [chain?.id]);

  useEffect(() => {
    if (!chain && isConnected && switchModal) {
      if (open) {
        setTimeout(() => {
          switchHandler();
        }, 100);
      }

      openSwitchNetworks();
    }
  }, [chain, isConnected, switchModal, open, switchHandler, openSwitchNetworks]);

  return (
    <>
      {isConnected
        ? (open && handleSwitchNetworkMessage(const_msg),
          currency_symbol !== default_currency ? (
            <SendToken clicked={clicked} />
          ) : (
            <SendnativeCurrency clicked={clicked} />
          ))
        : !isConnected && (
            <button
              type="button"
              className="button metamask-pay-items"
              onClick={() => setOpen(true)}
            >
              {label}
            </button>
          )}
    </>
  );
};

const queryClient = new QueryClient();

const App = (props) => {
  const [config, setConfig] = useState(null);
  const [showComponents, setShowComponents] = useState(false);
  const [switchModal, setSwitchModal] = useState(true);

  const updateSwitchModalHandler = () => {
    setSwitchModal(false);
  }

  const handleClick = () => {
    setShowComponents(true);
  };
  useEffect(() => {
    setConfig(createCustomConfig(ccpw_wc_id,selected_wallet,decimalchainId,network_data));
  }, []);

  return (
    <>
      {!showComponents && (
        <button
          type="button"
          className="button metamask-pay-items"
          onClick={handleClick}
        >
          {label}
        </button>
      )}
      {showComponents && config && (
         <WagmiProvider config={config}>
          <QueryClientProvider client={queryClient}>
          <ConnectKitProvider
            options={{
              walletConnectName: const_msg.wallet_connect,
              hideBalance: true,
            }}
          >
            <Checkcurrency clicked={showComponents} switchModal={switchModal} switchHandler={updateSwitchModalHandler} />
           </ConnectKitProvider>
          </QueryClientProvider>
        </WagmiProvider>
      )}
    </>
  );
};
export default App;
