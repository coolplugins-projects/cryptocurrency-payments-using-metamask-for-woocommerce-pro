import React, { useState, useEffect } from "react";
import {
  useEstimateGas,
  useSendTransaction,
  // useWaitForTransactionReceipt,
  useAccount,
  // useTransaction,
} from "wagmi";
import { parseEther } from "viem";
import {
  displayPopUp,
  // getDynamicTransactionData,
  validations,
  PaymentInProcess,
  ConfirmTransaction,
  TransactionRejected,
  RefundForm,
  handleSwitchNetworkMessage,
  RefundModalStyle
} from "../component/helper";
import {
  restApiSaveRefundTransaction,
  restApiRefundOrder,
} from "../component/handelRestApi";
import { useModal } from "connectkit";
import Modal from "react-modal";
const SendTransaction = ({ clicked }) => {
  const {
    recever,
    in_crypto,
    process_msg,
    wallet_image,
    const_msg,
    block_explorer,
    currency_symbol,
    connectedWallet,
    confirm_msg,
    fiat_symbol,
    in_fiat,
    network_name,
    last_price,
    rejected_msg,
    label,
    decimalchainId, // Add the correct network here
  } = extradata;
  const [refundAmount, setRefundAmount] = useState(in_crypto);
  const [refundReason, setRefundReason] = useState();
  const [calculatedFiatValue, setCalculatedFiatValue] = useState(
    (in_crypto * last_price).toFixed(2)
  );
  const [getSaveResponse, setSaveResponse] = useState(null);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [runOnce, setRunOnce] = useState(null);
  const { open, setOpen } = useModal();
  const { address, chain} = useAccount();
  const { data : estimateGas } = useEstimateGas({
    to: recever,
    value: parseEther(refundAmount),
  });

  const {sendTransaction, data, error } = useSendTransaction();

  if (open && chain?.id !== decimalchainId) {
    handleSwitchNetworkMessage(const_msg);
  }

  //Hide the chain change popup
  useEffect(() => {
    if (chain?.id === decimalchainId) {
      setOpen(false);
    }
  }, [chain?.id]);

  //Get initilas transaction details using hash
  // const saveHashResponse = useTransaction({ hash:data,refetchInterval:5  });

  //Save the initial transaction detilas in database
  useEffect(() => {
    // if (data && !runOnce && saveHashResponse.data) {
    if (data && !runOnce) {
      PaymentInProcess(
        wallet_image,
        process_msg,
        block_explorer,
        // saveHashResponse.data.hash,
        data,
        const_msg
      );

      // const response = getDynamicTransactionData(
      //   saveHashResponse.data,
      //   chain?.id,
      //   currency_symbol
      // );

      const staticData = {
        amount: in_crypto,
        chainId: chain?.id,
        from: address.toLowerCase(),
        hash: data,
        recever: recever.toLowerCase(),
        token_address: currency_symbol
      }

      restApiSaveRefundTransaction(
        // response,
        staticData, extradata)
      .then(function (backData) {
        setSaveResponse(backData);
        setRunOnce(true);
      });
    }
  // }, [data && saveHashResponse.data]);
  }, [data]);

    //Wait for transaction completetion
    // const waitFordata = useWaitForTransactionReceipt({
    //   hash:data,
    // });

    // Get confirmed transaction details using hash
    // const saveConfirmResponse = useTransaction({
    //   hash: waitFordata.data?.transactionHash,
    // });

  //Confirm the transaction & process order after block confirmation
  useEffect(() => {
    // if (waitFordata.data?.transactionHash && getSaveResponse) {
    if (data && getSaveResponse) {
      // const response = getDynamicTransactionData(
      //   saveConfirmResponse.data,
      //   chain?.id,
      //   currency_symbol
      // );
      const staticData = {
        amount: in_crypto,
        chainId: chain?.id,
        from: address.toLowerCase(),
        hash: data,
        recever: recever.toLowerCase(),
        token_address: currency_symbol
      }

      restApiRefundOrder(
        // response,
        staticData,
        getSaveResponse,
        extradata,
        calculatedFiatValue,
        refundReason
      );
    }
  // }, [saveConfirmResponse.data && getSaveResponse]);
  }, [data && getSaveResponse]);

  //if any error occur during payment process
  useEffect(() => {
      if (error) {
        if (error?.shortMessage === 'User rejected the request.') {
        TransactionRejected(rejected_msg, const_msg, wallet_image);
      } else {
        displayPopUp({
          msg: error?.shortMessage,
          image: wallet_image,
          time: 5000,
           });
         }
    }
  }, [error]);

  //Send transaction function handling
  const handleTransaction = () => {
    if (
      validations(
        calculatedFiatValue,
        const_msg,
        in_fiat,
        fiat_symbol,
        wallet_image
      ) !== true
    ) {
      return;
    } else {
      ConfirmTransaction(wallet_image, confirm_msg, const_msg);
          sendTransaction({
            gas: estimateGas,
            to:recever,
            value: parseEther(in_crypto),
          });
    }
  };

  const handleRefundAmountChange = (event) => {
    const value = event.target.value;

    setRefundAmount(value);

    // Calculate the corresponding fiat value here if needed
    // For example:
    const fiatValue = value * last_price;
    setCalculatedFiatValue(value ? fiatValue.toFixed(2) : 0);
  };

  const handleRefundReasonChange = (event) => {
    const value = event.target.value;
    setRefundReason(value);
  };

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  useEffect(() => {
    setIsOpen(true);
  }, [clicked]);
  Modal.setAppElement("#cpmwp_refund_btn");
  return (
    <>
      <button
        type="button"
        className="button metamask-pay-items"
        onClick={openModal}
      >
        {label}
      </button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={RefundModalStyle}
      >
        <RefundForm
          wallet_image={wallet_image}
          connectedWallet={connectedWallet}
          const_msg={const_msg}
          address={address}
          in_crypto={in_crypto}
          currency_symbol={currency_symbol}
          network_name={network_name}
          refundAmount={refundAmount}
          handleRefundAmountChange={handleRefundAmountChange}
          fiat_symbol={fiat_symbol}
          calculatedFiatValue={calculatedFiatValue}
          refundReason={refundReason}
          handleRefundReasonChange={handleRefundReasonChange}
          handleTransaction={handleTransaction}
          label={label}
        />
      </Modal>
    </>
  );
};
export default SendTransaction;
