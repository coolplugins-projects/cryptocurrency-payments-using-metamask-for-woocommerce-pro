import React, { useState, useEffect } from "react";
import * as wagmiChains from "wagmi/chains";
import Swal from "sweetalert2";
import { walletConnect, metaMask } from 'wagmi/connectors';
import { useAccount, createConfig, useBalance, useConnect } from "wagmi";
import ContentLoader from "react-content-loader";
import { formatUnits, Interface, formatEther } from "ethers";
import { ConnectKitButton, useModal, getDefaultConfig } from "connectkit";
import { getBalance } from "@wagmi/core";
import { etherUnits } from "viem";
import { restApiPrevPaymentStatus } from "./handelRestApi";
import { getTransactionReceipt, getTransaction } from '@wagmi/core'

export const isMobileDevice = () => {
  if (window.innerWidth < 500) {
    return true;
  } else {
    return false;
  }
};

//Component to import network by selected single id
export function importNetworkById(networkId, add_network) {
  try {
    const networkName = networkIdToName[networkId];
    if (!networkName) {
      return JSON.parse(add_network);
    }

    // eslint-disable-next-line no-prototype-builtins

    if (wagmiChains[networkName] !== undefined) {
      return wagmiChains[networkName];
    } else {
      throw new Error(
        `Network "${networkName}" is not available in wagmi/chains.`
      );
    }
  } catch (error) {
    console.error("Error importing network:", error);
    return null;
  }
}

//Component to display popup
export const displayPopUp = ({
  msg = false,
  icons = false,
  image = false,
  time = false,
  htmls = false,
  text = false,
  cancelbtn = false,
  showLoader = false,
  outsideClick = false,
  footer = false,
  closeBtn = false,
  confirmText = "Ok",
  endSession = false,
}) => {
  // Close any existing SweetAlert popups
  Swal.close();

  // Define the configuration object for SweetAlert
  const popupConfig = {
    title: msg,
    text: text,
    customClass: {
      container: "cpmwp_main_popup_wrap",
      popup: "cpmwp_popup",
    },
    icon: icons,
    html: htmls,
    showCancelButton: cancelbtn,
    confirmButtonColor: "#3085d6",
    confirmButtonText: confirmText,
    reverseButtons: true,
    imageUrl: image,
    footer: footer,
    timer: time,
    showCloseButton: closeBtn,
    showConfirmButton: false,
    allowOutsideClick: outsideClick,
    denyButtonText: "End Session",
    showDenyButton: endSession,
  };

  // Open SweetAlert popup and optionally show loading spinner
  const object = Swal.fire(popupConfig);
  if (showLoader) {
    Swal.showLoading();
  }

  return object;
};

//Change Switch network message
export const handleSwitchNetworkMessage = (const_msg, mobile) => {

  setTimeout(() => {
    const switchPage = document.querySelector(".sc-dcJsrY div");
    const switchNetworkmsg = document.querySelector(".sc-imWYAI");

    if (
      switchNetworkmsg &&
      switchPage.firstChild.textContent === "Switch Networks"
    ) {
      if (mobile) {
        switchNetworkmsg.textContent = const_msg.switch_network_mobile_msg;
      } else {
        switchNetworkmsg.textContent = const_msg.switch_network_msg;
      }
    }

  }, 50)

};

export const PaymentPendingUi = ({ txId, Address, wallet_image, connectedWallet, const_msg, address }) => {
  const message = const_msg['already_paid'].split('<transaction_hash/>');

  return (
    <>
      <div className="cpmw_payment_box cpmw_payment_pending">
        <div className="cpmw_p_header">
          <div className="cpmw_p_logo">
            <img src={wallet_image} />
          </div>
        </div>
        <div className="cpmw_p_title">{connectedWallet}</div>
        <div className="cpmw_p_connect">
          <div className="cpmw_p_status">{const_msg.connected}</div>
          <div className="cpmw_p_address">{address}</div>
        </div>
        <div className="cpmw_p_body">
          <div className="cpmw_p_desc">
           {message[0]} <b>{txId}</b> {message[1]}
          </div>
        </div>
      </div>
    </>
  )
}

//Ui component for process order page
export const PaymentUi = ({
  wallet_image,
  connectedWallet,
  const_msg,
  address,
  place_order_btn,
  without_discount,
  currency_symbol,
  in_crypto,
  network_name,
  currency_logo,
  fiatSymbol,
  totalFiat,
  handleTransaction,
  cancelOrder
}) => {
  return (
    <>
      <div className="cpmw_payment_box">
        <div className="cpmw_p_header">
          <div className="cpmw_p_logo">
            <img src={wallet_image} />
          </div>
          <div className="cpmw_p_title">{connectedWallet}</div>
        </div>
        <div className="cpmw_p_connect">
          <div className="cpmw_p_status">{const_msg.connected}</div>
          <div className="cpmw_p_address">{address}</div>
        </div>
        <div className="cpmw_p_body">
          <div className="cpmw_p_desc">
            {const_msg.pay_with}{" "}
            {without_discount && (
              <del>
                {without_discount} {currency_symbol}{" "}
              </del>
            )}
            {in_crypto} {currency_symbol} {const_msg.through} {network_name}{" "}
            {const_msg.to_complete}
          </div>
          <div className="cpmw_p_info">
            <div className="cpmw_p_amount">
              <img src={currency_logo} />
              {without_discount && (
                <del>
                  {without_discount} {currency_symbol}{" "}
                </del>
              )}
              {in_crypto} {currency_symbol}
              <span>
                ({fiatSymbol}
                {totalFiat})
              </span>
            </div>
            <div className="cpmw_p_network">{network_name} </div>
          </div>
          <div className="cpmw_p_button" onClick={handleTransaction}>
            {place_order_btn}
          </div>
          <div className="cpmw_p_note">
            {const_msg.cancel_order}{" "}
            <a onClick={() => cancelOrder(extradataRest, true)}>
              {const_msg.cancel_this_order}
            </a>{" "}
            {const_msg.create_new_one}
          </div>
        </div>
      </div>
    </>
  );
};

// RefundForm component for the refund form
export const RefundForm = ({
  refundAmount,
  handleRefundAmountChange,
  calculatedFiatValue,
  refundReason,
  handleRefundReasonChange,
  handleTransaction,
  fiat_symbol,
  wallet_image,
  connectedWallet,
  const_msg,
  address,
  currency_symbol,
  in_crypto,
  network_name,
  label,
}) => {
  return (
    <div className="cpmw_payment_box">
      <div className="cpmw_p_header">
        <div className="cpmw_p_logo">
          <img src={wallet_image} />
        </div>
        <div className="cpmw_p_title">{connectedWallet}</div>
      </div>
      <div className="cpmw_p_connect">
        <div className="cpmw_p_status">{const_msg.connected}</div>
        <div className="cpmw_p_address">{address}</div>
      </div>
      <div className="cpmw_p_body">
        <div className="cpmw_p_desc">
          {const_msg.pay_with} {in_crypto} {currency_symbol} {const_msg.through}{" "}
          {network_name} {const_msg.to_complete}
        </div>
        <div className="cpmw_p_info">
          <div className="cpmw_p_amount">
            <input
              type="text"
              className="swal2-input"
              id="refund_amount"
              value={refundAmount}
              placeholder="Refund amount"
              onChange={handleRefundAmountChange}
            />
            <span id="cpmw_custom"> {fiat_symbol + calculatedFiatValue}</span>
          </div>
          <div className="cpmw_p_network">
            {" "}
            <input
              type="text"
              className="swal2-input"
              value={refundReason}
              id="refund_reason"
              placeholder="Refund Reason"
              onChange={handleRefundReasonChange}
            />
          </div>
        </div>
        <div className="cpmw_p_button" onClick={handleTransaction}>
          {label}
        </div>
      </div>
    </div>
  );
};

//Transaction rejected popup component
export const TransactionRejected = (rejected_msg, const_msg, wallet_image) => {
  let html = `<div class="cpmw_popup trans_error">
	<div class="cpmw_pp_top">
		<div class="cpmw_pp_logo"><img src="${wallet_image}"/></div>
		<div class="cpmw_pp_title">${rejected_msg}</div>
		<div class="cpmw_pp_desc">${const_msg.rejected_msg}</div>
	</div>
	<div class="cpmw_pp_bottom">
		<div class="cpmw_pp_status">${const_msg.payment_status}</div>
		<div class="cpmw_pp_status_btn">${const_msg.failed}</div>
	</div>
</div>`;
  displayPopUp({ htmls: html, outsideClick: true, time: 1500 });
};
//Validatio popup message
export const ErrorPopupMsg = (rejected_msg, const_msg, wallet_image) => {
  let html = `<div class="cpmw_popup trans_error">
	<div class="cpmw_pp_top">
		<div class="cpmw_pp_logo"><img src="${wallet_image}"/></div>
		<div class="cpmw_pp_title">${rejected_msg}</div>		
	</div>
	<div class="cpmw_pp_bottom">
		<div class="cpmw_pp_status">${const_msg.payment_status}</div>
		<div class="cpmw_pp_status_btn">${const_msg.invalid}</div>
	</div>
</div>`;
  displayPopUp({ htmls: html, outsideClick: true, closeBtn: true });
};

//Validation for refund form
export const validations = (
  refund_amount,
  const_msg,
  in_fiat,
  fiat_symbol,
  wallet_image
) => {
  if (!refund_amount) {
    ErrorPopupMsg(const_msg.enter_amount, const_msg, wallet_image);
    return false;
  } else if (in_fiat == 0) {
    ErrorPopupMsg(const_msg.already_refunded, const_msg, wallet_image);
    return false;
  } else if (Number(refund_amount) > Number(in_fiat)) {
    ErrorPopupMsg(
      const_msg.refund_amount_notice + fiat_symbol + in_fiat,
      const_msg,
      wallet_image
    );
    return false;
  } else {
    return true;
  }
};
//Payment success popup
export const PaymentSuccessFull = (
  wallet_image,
  payment_msg,
  const_msg
) => {
  let html = `<div class="cpmw_popup trans_success">
	<div class="cpmw_pp_top">
		<div class="cpmw_pp_logo"><img src="${wallet_image}"/></div>
		<div class="cpmw_pp_title">${payment_msg}</div>
		<div class="cpmw_pp_desc">${const_msg.confirmed_payments_msg}</div>
	</div>
	<div class="cpmw_pp_bottom">
		<div class="cpmw_pp_status">${const_msg.payment_status}</div>
		<div class="cpmw_pp_status_btn">${const_msg.completed}</div>
	</div>
</div>`;
  displayPopUp({ htmls: html });
};

//Pyament in proccess popup message
export const PaymentInProcess = (
  wallet_image,
  process_msg,
  block_explorer,
  SendData,
  const_msg
) => {
  let html = `<div class="cpmw_popup trans_process">
	<div class="cpmw_pp_top">
		<div class="cpmw_pp_logo"><img src="${wallet_image}"/></div>
		<div class="cpmw_pp_title">${process_msg}</div>
		<div class="cpmw_pp_desc">${const_msg.payment_notice_msg}</div>
		<a href="${block_explorer}tx/${SendData}" target="_blank">${const_msg.check_in_explorer} &#8594;</a>
	</div>
	<div class="cpmw_pp_bottom">
	  <div class="cpmw_pp_status">${const_msg.payment_status}</div>
	  <div class="cpmw_pp_status_btn">${const_msg.in_process}</div>
	</div>
</div>`;
  displayPopUp({ htmls: html });
};
//Confirm transaction popup message
export const ConfirmTransaction = (wallet_image, confirm_msg, const_msg) => {
  let html = `<div class="cpmw_popup">
	<div class="cpmw_pp_top">
		<div class="cpmw_pp_logo"><img src="${wallet_image}"/></div>
		<div class="cpmw_pp_title">${confirm_msg}</div>
		<div class="cpmw_pp_desc">${const_msg.notice_msg}</div>
	</div>
	<div class="cpmw_pp_bottom">
		<div class="cpmw_pp_status">${const_msg.payment_status}</div>
		<div class="cpmw_pp_status_btn">${const_msg.pending}</div>
	</div>
</div>`;
  displayPopUp({ htmls: html });
};

//Fetch balance based on selected currency
export const FetchBalance = (props) => {
  const { connector: activeConnector, isConnected, address } = useAccount();
  const { open } = useModal();
  const { chain } = useAccount();
  const [balance, setBalance] = useState(null);
  const [data, setData] = useState(null);
  const [insufficientBalance, setInsufficientBalance] = useState(false);
  const selectedGateway = document.querySelector(
    'input[name="payment_method"]:checked'
  )?.value;
  const placeOrderButton = document.querySelector("button#place_order");

  // Function to fetch decimal balance
  const fetchDecimalBalance = async () => {
    try {
      if (address) {
        const result = await getBalance(props.config, {
          address: address,
          token: props.data
            ? props.data.networkResponse.contract_address[props.data.networks.id]
            : false,
        });
        setData(result);
      } else {
        setData(null)
      }
    } catch (error) {
      setData(null)
      console.log(error)
      // Handle errors gracefully
    }
  };

  // Fetch balance when connected and chain id matches
  useEffect(() => {
    if (isConnected) {
      if (chain?.id == props.data.networks.id) {
        fetchDecimalBalance();
      } else {
        setData(null);
      }
    }
  }, [isConnected, props.data.networks.id, chain?.id, open]);


  useEffect(() => {
    if (selectedGateway === "cpmw") {
      setTimeout(() => {
        if (data && isConnected) {
          const isInsufficient =
            parseFloat(data.formatted) <
            parseFloat(props.data.currentprice.rating);
          setInsufficientBalance(isInsufficient);
          placeOrderButton.disabled = isInsufficient;
        } else {
          setInsufficientBalance(false);
          placeOrderButton.disabled = true;
        }
      }, 100);
    } else {
      setInsufficientBalance(false);
      placeOrderButton.disabled = false;
    }
  }, [
    selectedGateway,
    data,
    props.data.currentprice?.rating,
    isConnected,
    placeOrderButton,
  ]);

  return (
    <>
      {isConnected ? (
        data !== null ? (
          <>
            {!insufficientBalance && (
              <div className="cpmwp_payment_notice">
                {props.const_msg.payment_notice}
              </div>
            )}
            {insufficientBalance && (
              <div className="cpmwp_insufficient_blnc">
                {props.const_msg.insufficent}
              </div>
            )}

            <input
              name="current_balance"
              type="hidden"
              value={data.formatted}
            />
            <input
              type="hidden"
              name="cpmwp_crypto_wallets"
              value={
                activeConnector?.id === "walletConnect"
                  ? "wallet_connect"
                  : "ethereum"
              }
            />
          </>
        ) : (
          <Loader loader={1} width={250} />
        )
      ) : null}
    </>
  );
};
//Loader component
export function PaymentLoader() {
  return <div className="ccpwp-card">
    <div className="ccpwp-card__image ccpwp-loading"></div>
    <div className="ccpwp-card__title ccpwp-loading"></div>
    <div className="ccpwp-card__description ccpwp-loading"></div>
  </div>;
}

//Loader component
export function Loader(props) {
  return [...Array(props.loader)].map((object, i) => {
    return (
      <ContentLoader
        key={i}
        height={40}
        width={1000}
        speed={2}
        style={{ width: "100%" }}
        backgroundColor={"#d9d9d9"}
        foregroundColor={"#ecebeb"}
      >
        <rect x="30" y="15" rx="4" ry="4" width="20" height="12.8" />
        <rect x="64" y="13" rx="6" ry="6" width="80%" height="18" />
      </ContentLoader>
    );
  });
}

//Generate transaction response data
export const getDynamicTransactionData = (
  details,
  Chainid,
  currency_symbol,
  decimals = 18
) => {
  if (details.input != undefined && details.input != "0x") {
    // Parse the input data to extract the function signature and parameters
    const iface = new Interface([
      "function transfer(address to, uint256 value)",
    ]);
    const parsedData = iface.parseTransaction({ data: details.input });
    if (parsedData.name === "transfer" && parsedData.args.length === 2) {
      const sentAmount = parsedData.args[1];
      return {
        amount: formatUnits(sentAmount, decimals),
        recever: parsedData.args[0],
        hash: details.hash,
        from: details.from,
        chainId: Chainid,
        token_address: details.to,
      };
    }
  } else {
    return {
      amount: formatEther(details.value),
      recever: details.to,
      hash: details.hash,
      from: details.from,
      chainId: Chainid,
      token_address: currency_symbol,
    };
  }
};

// Update prev transaction if status completed.
const fetchPrevTxStatus = async (hash, data, confirmTransFun, processData) => {
  const { wallet_image, process_msg, block_explorer, const_msg, config } = processData;
  try {
    // temp code for check transaction status pending
    const transactionReceipt = await getTransactionReceipt(config, {
      hash: hash,
    })

    if (transactionReceipt.status && 'success' === transactionReceipt.status) {
      const transactionData = await getTransaction(config, {
        hash: hash,
      })

      PaymentInProcess(
        wallet_image,
        process_msg,
        block_explorer,
        hash,
        const_msg
      );

      if (data.nonce && data.signature && data.order_id) {
        setTimeout(() => {
          confirmTransFun(transactionData, data);
        }, 2000);
      }
    }
    return true;
  } catch {
  }
}

// This function fetches previous transaction status and data
export const FetchPrevTx = async (txData, confirmTransFun, extradataRest, processData) => {
  const txHash = await restApiPrevPaymentStatus(txData, extradataRest).then(async (data) => {
    if (['pending','failed'].includes(data.transaction_status) && data.transaction_id && '' !== data.transaction_id) {
      const hash = data.transaction_id;

      const prevTxInterval = setInterval(async () => {
        const result = await fetchPrevTxStatus(hash, data, confirmTransFun, processData);
        if (result === true) {
          clearTimeout(prevTxInterval);
        }

      }, 5000);

      return hash;
    }
  })

  const responseData = { status: true };

  if (txHash) {
    responseData.txId = txHash;
  }

  return responseData;
}

//Connectkit custom connect button
export const CustomConnectButton = ({ const_msg }) => {
  const { setOpen } = useModal();

  return (
    <ConnectKitButton.Custom>
      {({ isConnected, show, truncatedAddress, ensName, address }) => (
        <>
          {isMobileDevice() && !isConnected ? setOpen(false) : null}
          <div onClick={show} className="cpmwp-connect-button">
            {isConnected
              ? ensName ?? truncatedAddress
              : const_msg
                ? const_msg.connect_wallet
                : "Connect Wallet"}
          </div>
        </>
      )}
    </ConnectKitButton.Custom>
  );
};

// ConnectKit custom btn for mobile
export const WalletConnectButtonMobile = ({ title, tag = "div", children }) => {

  const { connectors } = useConnect();
  const handleConnect = async () => {
    try {
      await connectors[0].connect();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      {"anchor" === tag ?
        <a onClick={handleConnect} className="cpmwp-connect-button">
          {title}{children}
        </a> :
        <div onClick={handleConnect} className="cpmwp-connect-button">
          {title}{children}
        </div>
      }
    </>
  );
};

export const createCustomConfig = (ccpw_wc_id, selected_wallet, decimalchainId, network_data) => {
  // Modify the default configuration based on props
  const customConfig = getDefaultConfig({
    walletConnectProjectId: ccpw_wc_id,
    //alchemyId: 'vE0lCPXbzgBGR3sU4Y68JHmBNsDYBf7S', // or infuraId
    appName: "Pay With Metamask",
    chains: [importNetworkById(decimalchainId, network_data)],
    appDescription: window.location.host,
    appUrl: window.location.host,
    appIcon: "https://family.co/logo.png",
  });

  if (customConfig) {
    const connectors = [];

    if (selected_wallet == "ethereum" && !isMobileDevice()) {
      connectors.push(metaMask()); // Assuming metaMask() is the connector function
    }
    if (selected_wallet == "wallet_connect" || isMobileDevice()) {
      if (ccpw_wc_id) {
        connectors.push(
          walletConnect({
            projectId: ccpw_wc_id,
            showQrModal: isMobileDevice(),
          })
        );
      }
    }

    customConfig.connectors = connectors;

    return createConfig(customConfig);
  }
}

//Refund modal style
export const RefundModalStyle = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    border: "unset",
    overflow: "visible",
    borderRadius: 0,
    outline: "none",
    padding: 0,
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

//Supported wagmi chains name
export const networkIdToName = {
  10: "optimism",
  11155111: "sepolia",
  42161: "arbitrum",
  1: "mainnet",
  137: "polygon",
  43114: "avalanche",
  5: "goerli",
  97: "bscTestnet",
  43113: "avalancheFuji",
  56: "bsc",
  80001: "polygonMumbai",
  14: "flare",
  16: "songbirdTestnet",
  19: "songbird",
  25: "cronos",
  40: "telos",
  41: "telosTestnet",
  50: "xdc",
  51: "xdcTestnet",
  57: "syscoin",
  66: "okc",
  100: "gnosis",
  114: "flareTestnet",
  240: "nexilix",
  250: "fantom",
  280: "zkSyncTestnet",
  288: "boba",
  314: "filecoin",
  324: "zkSync",
  338: "cronosTestnet",
  369: "pulsechain",
  420: "optimismGoerli",
  599: "metisGoerli",
  841: "taraxa",
  842: "taraxaTestnet",
  888: "wanchain",
  943: "pulsechainV4",
  997: "thunderTestnet",
  999: "wanchainTestnet",
  1038: "bronosTestnet",
  1039: "bronos",
  1088: "metis",
  1101: "polygonZkEvm",
  1284: "moonbeam",
  1285: "moonriver",
  1287: "moonbaseAlpha",
  1337: "localhost",
  1442: "polygonZkEvmTestnet",
  2000: "dogechain",
  2021: "edgeware",
  2022: "edgewareTestnet",
  3141: "filecoinHyperspace",
  3737: "crossbell",
  4002: "fantomTestnet",
  4242: "nexi",
  4689: "iotex",
  4690: "iotexTestnet",
  4759: "mevTestne",
  4777: "bxnTestnet",
  4999: "bxn",
  5000: "mantle",
  5001: "mantleTestnet",
  7518: "mev",
  7700: "canto",
  8082: "shardeumSphinx",
  8217: "klaytn",
  8453: "base",
  9000: "evmosTestnet",
  9001: "evmos",
  10200: "gnosisChiado",
  11235: "haqqMainnet",
  12306: "fibo",
  17323: "celoCannoli",
  31337: "foundry",
  42220: "celo",
  44787: "celoAlfajores",
  53935: "dfk",
  54211: "haqqTestedge2",
  59140: "lineaTestnet",
  84531: "baseGoerli",
  167005: "taikoTestnetSepolia",
  314159: "filecoinCalibration",
  421613: "arbitrumGoerli",
  534353: "scrollTestnet",
  1337803: "zhejiang",
  7777777: "zora",
  278611351: "skaleRazor",
  344106930: "skaleCalypsoTestnet",
  391845894: "skaleBlockBrawlers",
  476158412: "skaleEuropaTestnet",
  503129905: "skaleNebulaTestnet",
  1026062157: "skaleCryptoBlades",
  1273227453: "skaleHumanProtocol",
  1313161554: "aurora",
  1313161555: "auroraTestnet",
  1350216234: "skaleTitan",
  1351057110: "skaleChaosTestnet",
  1482601649: "skaleNebula",
  1517929550: "skaleTitanTestnet",
  1564830818: "skaleCalypso",
  1666600000: "harmonyOne",
  2046399126: "skaleCryptoColosseum",
  2139927552: "skaleExorde",
};


export const usdtAbi = [
  {
    type: 'event',
    name: 'Approval',
    inputs: [
      {
        indexed: true,
        name: 'owner',
        type: 'address',
      },
      {
        indexed: true,
        name: 'spender',
        type: 'address',
      },
      {
        indexed: false,
        name: 'value',
        type: 'uint256',
      },
    ],
  },
  {
    type: 'event',
    name: 'Transfer',
    inputs: [
      {
        indexed: true,
        name: 'from',
        type: 'address',
      },
      {
        indexed: true,
        name: 'to',
        type: 'address',
      },
      {
        indexed: false,
        name: 'value',
        type: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    name: 'allowance',
    stateMutability: 'view',
    inputs: [
      {
        name: 'owner',
        type: 'address',
      },
      {
        name: 'spender',
        type: 'address',
      },
    ],
    outputs: [
      {
        name: '',
        type: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    name: 'approve',
    stateMutability: 'nonpayable',
    inputs: [
      {
        name: 'spender',
        type: 'address',
      },
      {
        name: 'amount',
        type: 'uint256',
      },
    ],
    outputs: [
      {
        name: '',
        type: 'bool',
      },
    ],
  },
  {
    type: 'function',
    name: 'balanceOf',
    stateMutability: 'view',
    inputs: [
      {
        name: 'account',
        type: 'address',
      },
    ],
    outputs: [
      {
        name: '',
        type: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    name: 'decimals',
    stateMutability: 'view',
    inputs: [],
    outputs: [
      {
        name: '',
        type: 'uint8',
      },
    ],
  },
  {
    type: 'function',
    name: 'name',
    stateMutability: 'view',
    inputs: [],
    outputs: [
      {
        name: '',
        type: 'string',
      },
    ],
  },
  {
    type: 'function',
    name: 'symbol',
    stateMutability: 'view',
    inputs: [],
    outputs: [
      {
        name: '',
        type: 'string',
      },
    ],
  },
  {
    type: 'function',
    name: 'totalSupply',
    stateMutability: 'view',
    inputs: [],
    outputs: [
      {
        name: '',
        type: 'uint256',
      },
    ],
  },
  {
    type: 'function',
    name: 'transfer',
    stateMutability: 'nonpayable',
    inputs: [
      {
        name: 'recipient',
        type: 'address',
      },
      {
        name: 'amount',
        type: 'uint256',
      },
    ],
    outputs: [],
  },
  {
    type: 'function',
    name: 'transferFrom',
    stateMutability: 'nonpayable',
    inputs: [
      {
        name: 'sender',
        type: 'address',
      },
      {
        name: 'recipient',
        type: 'address',
      },
      {
        name: 'amount',
        type: 'uint256',
      },
    ],
    outputs: [
      {
        name: '',
        type: 'bool',
      },
    ],
  },
];
