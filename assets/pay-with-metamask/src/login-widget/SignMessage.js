import React, { useEffect } from "react";
import { useSignMessage, useAccount, useDisconnect } from "wagmi";
import { displayPopUp, isMobileDevice } from "../component/helper";
import { restApiSignMsg } from "../component/handelRestApi";

export function SignMessage(props) {
  const { address, isConnected } = useAccount();
  const {
    text_color,
    label_bg_color,
    sign_message,
    nonce,
    const_msg,
    logo_url,
    login_label,
    enable_disconnect,
  } = login_data;

  const msg = sign_message + nonce;
  const { signMessage, data, isSuccess, isLoading, error } = useSignMessage()
  
  const { disconnect } = useDisconnect();
  useEffect(() => {
    // Listen for changes in isConnected status
    if (isConnected) {
      setTimeout(() => {
        const hasConnectedBefore = localStorage.getItem(
          "CpmwphasConnectedBefore"
        );
        if (!hasConnectedBefore && !isMobileDevice()) {
          handleSignRequest();
          localStorage.setItem("CpmwphasConnectedBefore", "true");
        }
      }, 2000);
    } else {
      // Account has disconnected, remove the flag from local storage
      localStorage.removeItem("CpmwphasConnectedBefore");
    }
  }, [isConnected]);

  useEffect(() => {
    if (isSuccess) {
      restApiSignMsg(data, login_data, address);
    }
  
    if (error) {
      displayPopUp({
        msg: error.shortMessage,
        icons: "error",
        time: 2000,
      });
    }
    // Optional: Handle loading state if needed
    if (isLoading) {
      displayPopUp({
        msg: const_msg.process_request,
        image: logo_url,
        showLoader: true,
      });
    }
  }, [isSuccess, isLoading, data, error]);
  

  const handleSignRequest = () => {
    signMessage({ message: msg })
  };

  const StyledButton = {
    color: text_color,
    background: label_bg_color,
    borderColor: label_bg_color,
    boxShadow: `0 4px 24px -6px ${label_bg_color}`,
  };
  const StyledButtondisconnect = {
    color: text_color,
    borderColor: label_bg_color,
    background: label_bg_color,
    boxShadow: `0 4px 24px -6px ${label_bg_color}`,
  };

  return (
    <>
      <div className="ConnectWrap">
        <span
          className="cpmwp_sign_btn button button-primary"
          disabled={isLoading}
          onClick={handleSignRequest}
          style={StyledButton}
        >
          <span className="cpmpw_metamask_logo">
            <img src={logo_url} alt={login_label} />
            {login_label}
          </span>
        </span>
        {enable_disconnect && (
          <span
            className="cpmwp_disconnect button button-primary"
            title="Disconnect"
            style={StyledButtondisconnect}
            onClick={() => {
              disconnect();
              localStorage.removeItem("CpmwphasConnectedBefore");
            }}
          >
            <span class="dashicons dashicons-no"></span>
          </span>
        )}
      </div>
    </>
  );
}
