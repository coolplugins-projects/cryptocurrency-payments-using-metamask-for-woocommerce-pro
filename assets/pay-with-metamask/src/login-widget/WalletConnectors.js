import React, { useState, useEffect } from "react";
import { WagmiProvider, createConfig, useAccount,useConnect } from "wagmi";
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import {
  ConnectKitProvider,
  ConnectKitButton,
  getDefaultConfig,
  useModal,
} from "connectkit";
import { SignMessage } from "./SignMessage";
import * as wagmiChains from "wagmi/chains";
import { isMobileDevice } from "../component/helper";
import { walletConnect, metaMask } from 'wagmi/connectors';

// Define configuration constants
const labelBgColor = login_data.label_bg_color;
const textColor = login_data.text_color;
const logoUrl = login_data.logo_url;
const projectId = login_data.projectId;
const constMsg = login_data.const_msg;

const StyledButton = {
  color: '' === textColor ? "#fff" : textColor,
  background: '' === labelBgColor ? "#2271b1" : labelBgColor,
  borderColor: '' === labelBgColor ? "#2271b1" : labelBgColor,
  boxShadow: `0 4px 24px -6px ${'' === labelBgColor ? "#2271b1" : labelBgColor}`,
  padding: ".2rem .3rem",
  borderRadius: ".2rem",
};

// ConnectKit custom btn for mobile
export const WalletConnectButtonMobile = () => {

  const { connectors } = useConnect();
  const { address,isConnected } = useAccount();
  const handleConnect = async () => {
    try {
      await connectors[0].connect();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <span
        className="connect_wallets button button-primary"
        onClick={handleConnect}
        style={StyledButton}
      >
        {isConnected ? (
          address
        ) : (
          <span class="cpmpw_metamask_logo">
            <img src={logoUrl} alt="Logo" />
            {constMsg.connect_wallet}
          </span>
        )}
      </span>
    </>
  );
};

const CustomConnectButton = () => {
  const { setOpen } = useModal();
  return (
    <ConnectKitButton.Custom>
      {({ isConnected, show, truncatedAddress, ensName }) => {
        return (
          <>
            {isMobileDevice() && !isConnected ? setOpen(false) : null}
            <span
              className="connect_wallets button button-primary"
              onClick={show}
              style={StyledButton}
            >
              {isConnected ? (
                ensName ?? truncatedAddress
              ) : (
                <span class="cpmpw_metamask_logo">
                  <img src={logoUrl} alt="Logo" />
                  {constMsg.connect_wallet}
                </span>
              )}
            </span>
          </>
        );
      }}
    </ConnectKitButton.Custom>
  );
};

function HomePage() {
  const { isConnected } = useAccount();
  return isConnected ? <SignMessage /> : (!isMobileDevice() ? <CustomConnectButton /> : <WalletConnectButtonMobile/>);
}

const queryClient = new QueryClient();

function App() {
  const [config, setConfig] = useState(null);

  useEffect(() => {
    const customConfig = getDefaultConfig({
      walletConnectProjectId: projectId,
      appName: "Pay With Metamask",
      appDescription: window.location.host,
      chains: Object.values(wagmiChains),
      appUrl: window.location.host, // your app's URL
      appIcon: "https://family.co/logo.png", // your app's icon URL
    });
    const connectors = [];
    if (!isMobileDevice()) {
      connectors.push(metaMask());
      if (projectId) {
        connectors.push(walletConnect({
          projectId: projectId,
          showQrModal: false,
        }));
      }
    } else {
      if (projectId) {
        connectors.push(
          walletConnect({
            projectId: projectId,
            showQrModal: true,
          })
        );
      }
    }

    customConfig.connectors = connectors;

    setConfig(createConfig(customConfig));
  }, []);

  return (
    <>
      {config && wagmiChains && (
        <WagmiProvider config={config}>
          <QueryClientProvider client={queryClient}>
            <ConnectKitProvider
              options={{
                initialChainId: 0,
                walletConnectName: constMsg.wallet_connect,
                hideBalance: true,
              }}
            >
              <HomePage />
            </ConnectKitProvider>
          </QueryClientProvider>
        </WagmiProvider>
      )}
    </>
  );
}

export default App;
