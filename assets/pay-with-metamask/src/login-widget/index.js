import App from './WalletConnectors';
import { render } from '@wordpress/element';
import Swal from 'sweetalert2';
document.addEventListener( 'DOMContentLoaded', () => {
	if ( login_data.projectId == undefined || login_data.projectId == '' ) {
		render(
			<span
				className="button button-primary"
				onClick={ () =>
					Swal.fire( {
						title: login_data.const_msg.infura_msg,
						icon: 'warning',
					} )
				}
			>
				<span class="cpmpw_metamask_logo">
					<img src={ login_data.logo_url } />
					{ login_data.login_label }
				</span>
			</span>,
			document.getElementById( 'cpmwp_metamask_login' )
		);
	} else {
		render( <App />, document.getElementById( 'cpmwp_metamask_login' ) );
	}
} );
