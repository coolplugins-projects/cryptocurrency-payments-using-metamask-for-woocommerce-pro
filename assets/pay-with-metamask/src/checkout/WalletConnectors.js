import React, { useState, useEffect } from "react";
import {
  WagmiProvider,
  createConfig,
  useAccount,
  useDisconnect,
  http
} from "wagmi";
import { ConnectKitProvider, getDefaultConfig, useModal } from "connectkit";
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import {
  FetchBalance,
  CustomConnectButton,
  WalletConnectButtonMobile,
  isMobileDevice,
  handleSwitchNetworkMessage
} from "../component/helper";
import { walletConnect, metaMask } from 'wagmi/connectors';

const { ccpw_wc_id, enabledWallets, const_msg } = connect_wallts;

const BalanceAndConnect = ({ currentchain, config, switchModal, switchHandler }) => {
  const { open, setOpen, openSwitchNetworks } = useModal();
  const { isConnected, address, chain } = useAccount();
  const { disconnect } = useDisconnect();
  
  if (!chain && isConnected && switchModal) {
    if (open) {
      setTimeout(() => {
        switchHandler();
      }, 100)
    }

    openSwitchNetworks();
  }

  if (open) {
    handleSwitchNetworkMessage(const_msg)
  }

  useEffect(() => {
    if (chain?.id === currentchain.networks.id) {
      setOpen(false);
    }
  }, [chain?.id]);

  const handleDisconnect = () => {
    disconnect();
  };

  const renderConnectedInfo = () => (
    <>
      <div className="cpmw_p_connect">
        <div className="cpmw_p_status">{const_msg.connected}</div>
        <div className="cpmw_disconnect_wallet" onClick={handleDisconnect}>
          {const_msg.disconnect}
        </div>
      </div>
      <div className="cpmw_p_info">
        <div className="cpmw_address_wrap">
          <strong>{const_msg.wallet}:</strong>
          <span className="cpmw_p_address">{address}</span>
        </div>
        <div className="cpmw_p_network">
          <strong>{const_msg.network}:</strong>{" "}
          {currentchain.networkResponse.decimal_networks[chain?.id]
            ? currentchain.networkResponse.decimal_networks[chain?.id]
            : chain.name}
        </div>
      </div>
    </>
  );

  return (
    <>
      {chain && isConnected && renderConnectedInfo()}
      <FetchBalance data={currentchain} const_msg={const_msg} config={config}/>
      {(isMobileDevice() && !isConnected) && <WalletConnectButtonMobile title={const_msg.connect_wallet} />}
      {(!isMobileDevice() && !isConnected) && <CustomConnectButton const_msg={const_msg} />}
    </>
  );
};


const createCustomConfig = (props) => {

  const customConfig = getDefaultConfig({
    walletConnectProjectId: props.walletConnectProjectId,
    appName: props.appName,
    chains: [props.chains],
    appDescription: props.appDescription,
    appUrl: props.appUrl,
    appIcon: props.appIcon,
  });

  if (customConfig) {
    const connectors = [];

    Object.entries(enabledWallets).forEach((item) => {
      if (
        item[0] === "metamask_wallet" &&
        item[1] === "1" &&
        !isMobileDevice()
      ) {
        connectors.push(metaMask()); // Assuming metaMask() is the connector function
      } else if (
        (item[0] === "wallet_connect" && item[1] === "1") ||
        isMobileDevice()
      ) {

        if (props.walletConnectProjectId) {
          connectors.push(
            walletConnect({
              projectId: props.walletConnectProjectId,
              showQrModal: isMobileDevice(),
            })
          );
        }
      }
    });

    customConfig.connectors = connectors;

    return createConfig(customConfig);
  }
};


const queryClient = new QueryClient();

const App = (props) => {
  try {
    const [config, setConfig] = useState(null);
    const [switchModal, setSwitchModal] = useState(true);

    const updateSwitchModalHandler = () => {
      setSwitchModal(false);
    }

    useEffect(() => {
      // Define props to be passed to the config
      const configProps = {
        walletConnectProjectId: ccpw_wc_id,
        appName: "Pay With Metamask",
        appDescription: window.location.host,
        chains: props.networks,
        appUrl: window.location.host, // your app's URL
        appIcon: "https://family.co/logo.png", // your app's icon URL
      };
      setConfig(createCustomConfig(configProps));
    }, [props.networks]);

    return (
      [config && props.networks && (
        <div key="conn-app-wrapper">
          <WagmiProvider config={config}>
            <QueryClientProvider client={queryClient}>
              <ConnectKitProvider
                options={{
                  hideBalance: true,
                  walletConnectName: const_msg.wallet_connect,
                }}
              >
                <BalanceAndConnect currentchain={props} config={config} switchModal={switchModal} switchHandler={updateSwitchModalHandler} />
              </ConnectKitProvider>
            </QueryClientProvider>
          </WagmiProvider>
        </div>
      )]
    );
  } catch (error) {
    console.log(error);
  }
};

export default App;
