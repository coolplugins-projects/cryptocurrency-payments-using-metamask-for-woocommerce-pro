import React, { useState, useEffect } from "react";
import Connection from "./WalletConnectors";
import Select from "react-select";
import { importNetworkById, Loader } from "../component/helper";
import { restApiGetNetworks,restUpdatePrice } from "../component/handelRestApi";

export default function ShowCurrency() {
  const { enabledCurrency, const_msg, currency_lbl, network_lbl,total_price } =
    connect_wallts;

  const [selecteCurrency, setSelectedCurrency] = useState("");
  const [selectedNetwork, setSelectedNetwork] = useState(false);
  const [currentActiveNetwork, setCurrentActiveNetwork] = useState(null);
  const [selectedOption, setSelectedOption] = useState(null);
  const [enabledCurrencys, setEnabledCurrency] = useState(null);
  const [currencyChange, setCurrencyChange] = useState(false);
  const [networkChange, setNetworkChange] = useState(false);
  const [networkresponse, setNetworkResponse] = useState(false);
  const [oldPrice, setOldPrice] = useState(false);
  const [price, setPrice] = useState(total_price);


const numericPart = total_price 

     useEffect(() => {      

      if(numericPart){
        setEnabledCurrency(null);
        setSelectedOption(null);
        const numericValue = parseFloat(numericPart);
        if (!isNaN(numericValue)) {
          setPrice(numericValue);
          updateNewPrice(numericValue);
        } else {
          console.error("Parsed numeric part is not a valid number:", numericPart);
        }
      
      } 
    }, [numericPart]);
            

  const selectedGateway = document.querySelector(
    'input[name="payment_method"]:checked'
  )?.value;
 

  const placeOrderButton = document.querySelector("button#place_order");
 
  useEffect(() => {
    if (selectedGateway === "cpmw") {
      placeOrderButton.disabled = true;
    } else {
      placeOrderButton.disabled = false;
    }
  }, [selectedGateway]);
  


  useEffect(() => {
    createpriceOptions(enabledCurrency)
  }, []);
  //Handel network change event
  const handleNetworkChange = async (event) => {
    setCurrentActiveNetwork(null);
    setNetworkChange(true);
    const decimalChainId = parseInt(event?.value, 16);
    if (networkresponse.final_price[event.value]) {
      const price = networkresponse.final_price[event.value];
      const originalPrice = (
        <p className="">
          <img src={enabledCurrency[selecteCurrency].url} alt={event.value} />{" "}
          {networkresponse.token_discount[decimalChainId] && (
            <del>
              {enabledCurrency[selecteCurrency].price}{" "}
              {enabledCurrency[selecteCurrency].symbol}
            </del>
          )}{" "}
          {price} {enabledCurrency[selecteCurrency].symbol}
        </p>
      );

      const newprice = {
        value: selecteCurrency,
        label: originalPrice,
        rating: price,
      };

      setSelectedOption(newprice);
    } else {
      setSelectedOption(oldPrice);
    }

    const res = await importNetworkById(
      decimalChainId,
      networkresponse.network_settings[event?.value]
    );

    setNetworkChange(false);
    setCurrentActiveNetwork(res);
  };
  //Handel currency change event
  const handleCurrencyChange = (event) => {
    setCurrencyChange(true);
    setOldPrice(event);
    setSelectedOption(event);
    GenerateNetworkHtml(event.value);
    setSelectedCurrency(event.value);
    setCurrentActiveNetwork(null);
    placeOrderButton.disabled = true;
  };
  const updateNewPrice = async (symbol) => {
    try {
      const response = await restUpdatePrice(symbol, connect_wallts);
      if (response) {
        createpriceOptions(response);
        connect_wallts.enabledCurrency = response;
      } else {
        console.error("No response or undefined response from restUpdatePrice");
      }

    
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const createpriceOptions = async (enabledCurrency) => {
    try {
      let currency = [];

      //if (enabledCurrency.length !== 0) {
      if(enabledCurrency && Object.keys(enabledCurrency).length !== 0){
        Object.values(enabledCurrency).forEach((value) => {
          if (!value.price) {
            return;
          }
          const chainData = {
            value: value.symbol,
            label: (
              <p className="">
                <img src={value.url} alt={value.symbol} /> {value.price}{" "}
                {value.symbol}
              </p>
            ),
            rating: value.price,
          };
          currency.push(chainData);
        });
        setEnabledCurrency(currency);
      }
    
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const GenerateNetworkHtml = async (symbol) => {
    try {      
      const response = await restApiGetNetworks(symbol, connect_wallts,price);
      const networks = [];

      setNetworkResponse(response);

      Object.entries(response?.active_network).forEach((item) => {
        const chainData = {
          value: item[0],
          label: item[1],
        };
        networks.push(chainData);
      });

      setSelectedNetwork(networks);
      setCurrencyChange(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  return (
    <div key="show_currency_wrapper">{enabledCurrencys ? (
        <>
          <div className="cpmwp_currency_lbl">{currency_lbl}</div>
          <Select
            name="cpmwp_crypto_coin"
            value={selectedOption}
            onChange={handleCurrencyChange}
            options={enabledCurrencys}
            placeholder={const_msg.select_cryptocurrency}
          />

          {selectedNetwork ? (
            <>
              {currencyChange ? (
                <Loader loader={1} width={250} />
              ) : (
                <>
                  <div className="cpmwp_network_lbl">{network_lbl}</div>
                  <Select
                    name="cpmw_payment_network"
                    onChange={handleNetworkChange}
                    options={selectedNetwork}
                    placeholder={const_msg.choose_network_chain}
                  />
                </>
              )}
            </>
          ) : (
            currencyChange && <Loader loader={1} width={250} />
          )}

          {currentActiveNetwork && (
            <Connection
              networks={currentActiveNetwork}
              currentprice={selectedOption}
              networkChange={networkChange}
              networkResponse={networkresponse}
            />
          )}
        </>
      ) : (
        <Loader loader={1} width={250} />
      )}</div>
  );
}
