import React, { useState, useEffect } from 'react';
import { WagmiProvider, useAccount } from 'wagmi';
import { ConnectKitProvider, useModal } from 'connectkit';
import { handleSwitchNetworkMessage, isMobileDevice } from '../component/helper';
import SendnativeCurrency from './sendNativeCurrency';
import SendToken from './sendToken';
import { createCustomConfig, WalletConnectButtonMobile } from '../component/helper';
import { cancelOrder } from '../component/handelRestApi';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const { const_msg, currency_symbol, default_currency, wallet_image, selected_wallet, decimalchainId, place_order_btn, connectedWallet,
	without_discount, ccpw_wc_id, network_data, network_name, in_crypto, fiatSymbol, currency_logo, totalFiat } = extradataRest;

const Checkcurrency = ({ config }) => {
	const { isConnected } = useAccount();
	const { open, setOpen } = useModal();

	if (isConnected) {
		if (open) {
			handleSwitchNetworkMessage(const_msg)
		}
		if (currency_symbol !== default_currency) {
			return <SendToken config={config} />
		}
		else {
			return <SendnativeCurrency config={config} />
		}
	}
	else {
		return (<>
			<div className="cpmw_payment_box">
				<div className="cpmw_p_header">
					<div className="cpmw_p_logo"><img src={wallet_image} /></div>
					<div className="cpmw_p_title">{connectedWallet}</div>
				</div>
				<div className="cpmw_p_connect">
					<div className="cpmw_p_status no_connect">{const_msg.not_connected}</div>
					<div className="cpmw_p_address" >
						{isMobileDevice() ?
							<WalletConnectButtonMobile title={const_msg.connect_wallet} tag="anchor">&#8594;</WalletConnectButtonMobile>
							:
							<a className='cpmwp-connect-button' onClick={() => setOpen(true)}>{const_msg.connect_wallet} &#8594;</a>
						}
					</div>
				</div>
				<div className="cpmw_p_body">
					<div className="cpmw_p_desc">{const_msg.pay_with} {without_discount && (<del>{without_discount} {currency_symbol} </del>)}
						{in_crypto} {currency_symbol} {const_msg.through} {network_name} {const_msg.to_complete}</div>
					<div className="cpmw_p_info">
						<div className="cpmw_p_amount"><img src={currency_logo} />{without_discount && (<del>{without_discount} {currency_symbol} </del>)}
							{in_crypto} {currency_symbol}<span>({fiatSymbol}{totalFiat})</span></div>
						<div class="cpmw_p_network">{network_name}</div>
					</div>
					<div className="cpmw_p_button no_connect" title="Connect Wallet!">{place_order_btn}</div>
					<div className="cpmw_p_note">{const_msg.cancel_order} <a onClick={() => cancelOrder(extradataRest, true)}>{const_msg.cancel_this_order}</a> {const_msg.create_new_one}</div>
				</div>
			</div>

		</>)
	}
}

const queryClient = new QueryClient();

const App = (props) => {
	const [config, setConfig] = useState(null);
	useEffect(() => {
		setConfig(createCustomConfig(ccpw_wc_id, selected_wallet, decimalchainId, network_data));
	}, []);

	return (
		<>
			{config && (
				<WagmiProvider config={config}>
					<QueryClientProvider client={queryClient}>
						<ConnectKitProvider
							options={{
								walletConnectName: const_msg.wallet_connect,
								hideBalance: true,
							}}
						>
							<Checkcurrency config={config} />
						</ConnectKitProvider>
					</QueryClientProvider>
				</WagmiProvider>
			)}
		</>
	);
};
export default App;

