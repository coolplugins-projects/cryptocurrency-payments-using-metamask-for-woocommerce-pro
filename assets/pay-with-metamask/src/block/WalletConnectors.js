
import { useState, useEffect } from '@wordpress/element';
import {
  WagmiProvider,
  createConfig,
  useAccount,
  useDisconnect,
} from "wagmi";
import { ConnectKitProvider, getDefaultConfig, useModal } from "connectkit";
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import {
  CustomConnectButton,
  isMobileDevice,
  handleSwitchNetworkMessage,
  Loader,
  WalletConnectButtonMobile
} from "../component/helper";
import { getBalance } from "@wagmi/core";
const settings = window.wc.wcSettings.getPaymentMethodData('cpmw');
const { ccpw_wc_id, enabledWallets, const_msg } = settings;
import { walletConnect,metaMask } from 'wagmi/connectors';

// Component to handle balance and connection
const BalanceAndConnect = ({ currentchain, config, switchModal, switchHandler }) => {
  const { open, setOpen,openSwitchNetworks } = useModal();
  const { isConnected, address,chain } = useAccount();
  const { disconnect } = useDisconnect();

  if(!chain && isConnected && switchModal){
    if(open){
      setTimeout(()=>{
        switchHandler();
      },100)
    }
    
    openSwitchNetworks();
  }
  
  // Handle network switch message
  if (open) {
    handleSwitchNetworkMessage(const_msg, isMobileDevice())
  }

  // Close modal if chain id matches current chain id
  useEffect(() => {
    if (chain?.id === currentchain.networks.id) {
      setOpen(false);
    }
  }, [chain?.id]);

  // Disconnect handler
  const handleDisconnect = () => {
    disconnect();
  };

  // Component to fetch balance based on selected currency
  const FetchBalanceblock = (props) => {
    const { connector: activeConnector, isConnected, address } = useAccount();
    const { chain } = useAccount();

    const [balance, setBalance] = useState(null);
    const [insufficientBalance, setInsufficientBalance] = useState(false);

    // Function to fetch decimal balance
    const fetchDecimalBalance = async () => {
      try {
        if(address){
          const result = await getBalance(config, {
            address: address,
            token: props.data
              ? props.data.networkResponse.contract_address[props.data.networks.id]
              : false,
          });
          props.data.getbalance(result, activeConnector?.id === "walletConnect" ? "wallet_connect" : "ethereum", isConnected)
          setBalance(result);
        }
      } catch (error) {
        console.log(error)
        // Handle errors gracefully
      }
    };

    // Fetch balance when connected and chain id matches
    useEffect(() => {
      if (isConnected) {
        setBalance(null);
        if (chain?.id == props.data.networks.id) {
          fetchDecimalBalance();
        }
      }
      else {
        props.data.getbalance(balance, activeConnector?.id === "walletConnect" ? "wallet_connect" : "ethereum", isConnected)
      }
    }, [isConnected, props.data.networks.id, chain?.id,open]);

    // Check for insufficient balance
    useEffect(() => {
      setTimeout(() => {
        if (balance && isConnected) {
          const isInsufficient = parseFloat(balance.formatted) < parseFloat(props.data.currentprice.rating);
          setInsufficientBalance(isInsufficient);
        } else {
          setInsufficientBalance(false);
        }
      }, 100);

    }, [
      balance,
      props.data.currentprice?.rating,
      isConnected,
    ]);

    // Render balance information
    return (
      <>
        {isConnected ? (
          balance !== null ? (
            <>
              {!insufficientBalance && (
                <div className="cpmwp_payment_notice">
                  {props.const_msg.payment_notice}
                </div>
              )}
              {insufficientBalance && (
                <div className="cpmwp_insufficient_blnc">
                  {props.const_msg.insufficent}
                </div>
              )}
            </>
          ) : (
            <Loader loader={1} width={250} />
          )
        ) : null}
      </>
    );
  };

  // Render connected wallet information
  const renderConnectedInfo = () => (
    <>
      <div className="cpmw_p_connect">
        <div className="cpmw_p_status">{const_msg.connected}</div>
        <div className="cpmw_disconnect_wallet" onClick={handleDisconnect}>
          {const_msg.disconnect}
        </div>
      </div>
      <div className="cpmw_p_info">
        <div className="cpmw_address_wrap">
          <strong>{const_msg.wallet}:</strong>
          <span className="cpmw_p_address">{address}</span>
        </div>
        <div className="cpmw_p_network">
          <strong>{const_msg.network}:</strong>{" "}
          {currentchain.networkResponse.decimal_networks[chain?.id]
            ? currentchain.networkResponse.decimal_networks[chain?.id]
            : chain.name}
        </div>
      </div>
    </>
  );

  return (
    <>
      {chain && isConnected && renderConnectedInfo()}
      <FetchBalanceblock data={currentchain} const_msg={const_msg} />
      {(isMobileDevice() && !isConnected) && <WalletConnectButtonMobile title={const_msg.connect_wallet}/>}
      {(!isMobileDevice() && !isConnected) && <CustomConnectButton const_msg={const_msg}/>}
    </>
  );
};

// Function to create custom config
const createCustomConfig = (props) => {
  const customConfig = getDefaultConfig({
    walletConnectProjectId: props.walletConnectProjectId,
    appName: props.appName,
    chains: [props.chains],
    appDescription: props.appDescription,
    appUrl: props.appUrl,
    appIcon: props.appIcon,
  });

  if (customConfig) {
    const connectors = [];

    Object.entries(enabledWallets).forEach((item) => {
      if (
        item[0] === "metamask_wallet" &&
        item[1] === "1" &&
        !isMobileDevice()
      ) {
        connectors.push(metaMask()); // Assuming metaMask() is the connector function
      } else if (
        (item[0] === "wallet_connect" && item[1] === "1") ||
        isMobileDevice()
      ) {
      
        if (props.walletConnectProjectId) {
          connectors.push(
            walletConnect({
              projectId: props.walletConnectProjectId,
              showQrModal: isMobileDevice(),
            })
          );
        }
      }
    });

    customConfig.connectors = connectors;

    return createConfig(customConfig);
  }
};

const queryClient = new QueryClient();

// Main App component
const App = (props) => {
  try {
    const [config, setConfig] = useState(null);
    const [switchModal,setSwitchModal]=useState(true);
    
    const updateSwitchModalHandler=()=>{
      setSwitchModal(false);
    }
    // Create custom config on component mount
    useEffect(() => {
      const configProps = {
        walletConnectProjectId: ccpw_wc_id,
        appName: "Pay With Metamask",
        appDescription: window.location.host,
        chains: props.networks,
        appUrl: window.location.host,
        appIcon: "https://family.co/logo.png",
      };

      setConfig(createCustomConfig(configProps));
    }, [props.networks]);
    
    // Render App
    return (
      [config && props.networks && (
        <div key="conn-app-wrapper">
          <WagmiProvider config={config}>
            <QueryClientProvider client={queryClient}>
              <ConnectKitProvider
                options={{
                  hideBalance: true,
                  walletConnectName: const_msg.wallet_connect,
                }}
              >
                <BalanceAndConnect currentchain={props} config={config} switchModal={switchModal} switchHandler={updateSwitchModalHandler}/>
              </ConnectKitProvider>
            </QueryClientProvider>
          </WagmiProvider>
        </div>
      )]
    );
  } catch (error) {
    console.log(error);
  }
};

export default App;
