// Importing necessary modules from WordPress
import { sprintf, __ } from '@wordpress/i18n';
// Importing the CurrencyAndNetworkManager component
import Content from'./CurrencyAndNetworkManager';

(function ( React ) {
  // Fetching the settings from the window object
  const settings = window.wc.wcSettings.getPaymentMethodData( 'cpmw' );
  
  // Creating a functional component for the PaymentBlockLabel
  const PaymentBlockLabel = () => {
    return(
      <>
        <div>{ settings.title }</div>
        <div style={{ flexGrow: "1", display: 'flex', justifyContent: 'end', paddingRight: '14px', paddingLeft: '14px' }}>
          <img  title={settings.title} style={{ marginBottom: '2px', width: 'auto', height: '28px' }} src={settings.logo_url} />	
        </div>
      </>
    )
  }
  
  // Registering the payment method with the necessary details
  window.wc.wcBlocksRegistry.registerPaymentMethod({
    name: `cpmw`,
    label: Object( window.wp.element.createElement )( PaymentBlockLabel, null ),
    ariaLabel: settings.title,
    content: settings.error?<>{settings.error}</>:<Content />,
    edit: settings.error?<>{settings.error}</>:<Content />,
    placeOrderButtonLabel: settings.order_button_text,
    canMakePayment: () => true,
    paymentMethodId: `cpmw`,
    supports: {
      features: settings.supports		
    }
  })
})( window.React )
