import React, { useState, useEffect } from "react";
import Connection from "./WalletConnectors";
import Select from "react-select";
import { importNetworkById, Loader } from "../component/helper";
import { restApiGetNetworks,restUpdatePrice } from "../component/handelRestApi";

// Main function for showing currency
export default function ShowCurrency({ eventRegistration,emitResponse,billing }) {
  const settings = window.wc.wcSettings.getPaymentMethodData( 'cpmw' );
  // Destructuring props
	const { onPaymentSetup } = eventRegistration;
  // Destructuring settings
  const { enabledCurrency, const_msg, currency_lbl, network_lbl,total_price } =
    settings;

  // State variables for managing currency and network selection
  const [selecteCurrency, setSelectedCurrency] = useState("");
  const [selectedNetwork, setSelectedNetwork] = useState(false);
  const [currentActiveNetwork, setCurrentActiveNetwork] = useState(null);
  const [selectedOption, setSelectedOption] = useState(null);
  const [enabledCurrencys, setEnabledCurrency] = useState(null);
  const [currencyChange, setCurrencyChange] = useState(false);
  const [networkChange, setNetworkChange] = useState(false);
  const [networkresponse, setNetworkResponse] = useState(false);
  const [oldPrice, setOldPrice] = useState(false);
  const [price, setPrice] = useState(total_price);
  const [activeNetwrok, setactiveNetwrok] = useState();
  const [currenctBalance, setcurrenctBalance] = useState();
  const [activeWallet ,setactiveWallet] = useState();
  const [walletConnected ,setwalletConnected ]= useState(false);

  // Function to format currency
  const formatCurrency = (value, currency) => {    
    const { minorUnit } = currency;
    const formattedValue = (value / Math.pow(10, minorUnit)).toFixed(minorUnit);
    return formattedValue;
  };
  const numericPart = formatCurrency(billing.cartTotal.value,billing.currency);
  // Effect to update price on numeric part change
  useEffect(() => {      
    if(numericPart){
      setEnabledCurrency(null);
      setSelectedOption(null)
      setPrice(Number(numericPart))
      updateNewPrice(Number(numericPart))    
    } 
  }, [numericPart]);

  // Effect to create price options on enabledCurrency change
  useEffect(() => {
    createpriceOptions(enabledCurrency)
  }, []);

  // Function to handle network change event
  const handleNetworkChange = async (event) => {
    setCurrentActiveNetwork(null);
    setNetworkChange(true);
    const decimalChainId = parseInt(event?.value, 16);
    if (networkresponse.final_price[event.value]) {
      const price = networkresponse.final_price[event.value];
      const originalPrice = (
        <span className="cpmwp_logos">
          <img src={enabledCurrency[selecteCurrency].url} alt={event.value} style={{ width: 'auto', height: '28px' }}/>{" "}
          {networkresponse.token_discount[decimalChainId] && (
            <del>
              {enabledCurrency[selecteCurrency].price}{" "}
              {enabledCurrency[selecteCurrency].symbol}
            </del>
          )}{" "}
          {price} {enabledCurrency[selecteCurrency].symbol}
        </span>
      );

      const newprice = {
        value: selecteCurrency,
        label: originalPrice,
        rating: price,
      };

      setSelectedOption(newprice);
    } else {
      setSelectedOption(oldPrice);
    }

    const res = await importNetworkById(
      decimalChainId,
      networkresponse.network_settings[event?.value]
    );

    setactiveNetwrok(event?.value)
    setNetworkChange(false);
    setCurrentActiveNetwork(res);
  };

  // Function to handle currency change event
  const handleCurrencyChange = (event) => {
    setCurrencyChange(true);
    setOldPrice(event);
    setSelectedOption(event);
    GenerateNetworkHtml(event.value);
    setSelectedCurrency(event.value);
    setCurrentActiveNetwork(null);
  };

  // Function to update new price
  const updateNewPrice = async (symbol) => {
    try {
      const response = await restUpdatePrice(symbol, settings);
      createpriceOptions(response)
      settings.enabledCurrency=response;
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Function to create price options
  const createpriceOptions = async (enabledCurrency) => {
    try {
      let currency = [];
      if (enabledCurrency && Object.keys(enabledCurrency).length !== 0) {
        Object.values(enabledCurrency).forEach((value) => {
          if (!value.price) {
            return;
          }
          const chainData = {
            value: value.symbol,
            label: (
              <span className="cpmwp_logos">
                <img src={value.url} alt={value.symbol} style={{ width: 'auto', height: '28px' }} /> {value.price}{" "}
                {value.symbol}
              </span>
            ),
            rating: value.price,
          };
          currency.push(chainData);
        });
        setEnabledCurrency(currency);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Function to generate network HTML
  const GenerateNetworkHtml = async (symbol) => {
    try {      
      const response = await restApiGetNetworks(symbol, settings,price);
      const networks = [];

      setNetworkResponse(response);

      Object.entries(response?.active_network).forEach((item) => {
        const chainData = {
          value: item[0],
          label: item[1],
        };
        networks.push(chainData);
      });

      setSelectedNetwork(networks);
      setCurrencyChange(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Function to get balance of active network
  const getBalanceofActiveNetwork=(balance,wallet,connected)=>{
    setcurrenctBalance(balance&&balance.formatted)  
    setactiveWallet(wallet)   
    setwalletConnected(connected)  
  }

  // Effect to handle payment setup
	useEffect( () => {
    const unsubscribe = onPaymentSetup(async () => {	       

    if(!selecteCurrency){
      return {
				type: emitResponse.responseTypes.ERROR,
				message: settings.const_msg.required_currency,
			};
    }
    else if(!activeNetwrok){
      return {
				type: emitResponse.responseTypes.ERROR,
				message: settings.const_msg.required_network_check,
			};
    }
    else if(!walletConnected){
      return {
				type: emitResponse.responseTypes.ERROR,
				message: settings.const_msg.connect_wallet,
			};
    }
    else if(Number(currenctBalance) < Number(selectedOption?.rating.replace(/,/g, ''))){
      return {
				type: emitResponse.responseTypes.ERROR,
				message: settings.const_msg.insufficent,
			};
    } 
			if ( currenctBalance&&activeWallet ) {
				return {
					type: emitResponse.responseTypes.SUCCESS,
					meta: {
						paymentMethodData: {							
							cpmwp_crypto_coin:selecteCurrency,
							cpmw_payment_network: activeNetwrok,
							current_balance: currenctBalance,
							cpmwp_crypto_wallets: activeWallet,         
						},
					},
				};
			}

      return {
				type: emitResponse.responseTypes.ERROR,
				message:"Something went wrong",
			};

		} );
		// Unsubscribes when this component is unmounted.
		return () => {
			unsubscribe();
		};
	}, [
		emitResponse.responseTypes.ERROR,
		emitResponse.responseTypes.SUCCESS,
		onPaymentSetup,
    currenctBalance,
    activeWallet,
    selecteCurrency,
    walletConnected
	] );

  // Render function
  return (
    <div key="show-currency-wrapper">
      {enabledCurrencys ? (
        <div className="cpmwp-supported-wallets-wrap">
          {settings.description && <span className="description" key="description">{settings.description}</span>}
          <div className="cpmwp_currency_lbl" key="currency-label">
            {currency_lbl}
          </div>
          <Select
            name="cpmwp_crypto_coin"
            value={selectedOption}
            onChange={handleCurrencyChange}
            options={enabledCurrencys}
            placeholder={const_msg.select_cryptocurrency}
            key="crypto-select"
          />
  
          {selectedNetwork ? (
            <>
              {currencyChange ? (
                <Loader loader={1} width={250} key="loader" />
              ) : (
                <>
                  <div className="cpmwp_network_lbl" key="network-label">
                    {network_lbl}
                  </div>
                  <Select
                    name="cpmw_payment_network"
                    onChange={handleNetworkChange}
                    options={selectedNetwork}
                    placeholder={const_msg.choose_network_chain}
                    key="network-select"
                  />
                </>
              )}
            </>
          ) : (
            currencyChange && <Loader loader={1} width={250} key="loader-currency" />
          )}
  
          {currentActiveNetwork && (
            <Connection           
              getbalance={getBalanceofActiveNetwork}
              networks={currentActiveNetwork}
              currentprice={selectedOption}
              networkChange={networkChange}
              networkResponse={networkresponse}
              key="connection-component"
            />
          )}
        </div>
      ) : (
        <Loader loader={1} width={250} key="loader-supported-wallets" />
      )}
    </div>
  );
}
